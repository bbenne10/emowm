#! /usr/bin/env python2
# -*- coding:utf-8 -*-
from __future__ import division
import Xlib
from Xlib import X, display, XK, Xutil
from Xlib import Xcursorfont as Xcf
from keysymdef import keysyms
from subprocess import call
import operator
from os import spawnvp,P_NOWAIT
from itertools import chain, combinations
from User_Config import User_Config 


class Client():
    def __init__(self, window):
        self.window = window
        self.monitor = 0
        self.tags = 0
        self.title = window.get_wm_name()
        window_geom = window.get_geometry()
        self.x = window_geom.x
        self.y = window_geom.y
        self.old_x = 0
        self.old_y = 0

        self.width = window_geom.width
        self.height = window_geom.height
        del(window_geom)

        self.is_master = False
        self.is_visible = False
        self.is_floating = False
        self.is_focused = False
    def __str__(self):
        return "< Client instance: {0} >".format(self.title)

        # more here

#class Monitor():
#    def __init__(self, dpy):
#        self.dpy = dpy
#        self.width = dpy.screen().width_in_pixels
#        self.height = dpy.screen().height_in_pixels
#        self.bar_win = False
#        self.clients = []


class Draw_Context():
    def __init__(self, x=None, y=None, w=None, h=None, colors=None,
            drawable=None, gc=None, pgc=None, plo=None,
            pfd=None, font=None):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        if colors == None:
            self.colors = []
        else:
            self.colors = colors
        self.drawable = drawable
        self.gc = gc
        self.pgc = pgc
        self.plo = plo
        self.pfd = pfd
        self.font = font
        self.font_properties = {}


class Window_Manager:
    def __init__(self):
        self.dpy = display.Display()

        # FOR TESTING ONLY
        x = self.dpy.get_font_path()
        x.append('/usr/share/fonts/local')
        self.dpy.set_font_path(x)

        self.screen = self.dpy.screen()
        self.root = self.dpy.screen().root
        error_catcher = Xlib.error.CatchError(Xlib.error.BadAccess)
        self.root.change_attributes(event_mask =    X.SubstructureNotifyMask |
                                                    X.KeyPressMask |
                                                    X.FocusChangeMask |
                                                    X.EnterWindowMask,
                                    on_error = error_catcher)

        self.dpy.sync()
        error = error_catcher.get_error()
        if error:
            print "Unable to set SubstructureNotify!"
            exit(2)
        self.config = User_Config()
        self.bar_window = None
        self.screen_width   = self.screen.width_in_pixels
        self.screen_height  = self.screen.height_in_pixels
        self.current_layout = 0         #TODO: add support for multiple layouts on multiple tags
        self.current_tag    = 0         #TODO: add support for multiple tags on multiple screens
        self.draw_context = Draw_Context()
        self.cursor = {}

        self.clients = dict((tag, []) for tag in self.config.tags)

        # initialize cursor list
        cf = self.dpy.open_font('cursor')
        self.cursor['CurNormal'] = cf.create_glyph_cursor(cf,
            Xcf.left_ptr, Xcf.left_ptr + 1,
            (0, 0, 0), (0xFFFF, 0xFFFF, 0xFFFF))
        self.cursor['CurResize'] = cf.create_glyph_cursor(cf,
            Xcf.sizing, Xcf.sizing + 1,
            (0, 0, 0), (0xFFFF, 0xFFFF, 0xFFFF))
        self.cursor['CurMove'] = cf.create_glyph_cursor(cf,
            Xcf.fleur, Xcf.fleur + 1,
            (0, 0, 0), (0xFFFF, 0xFFFF, 0xFFFF))

        # initfont() in dwm.c
        self.font = self.dpy.open_font(self.config.font)
        if self.font == None:
            print("Failed to allocate provided font - trying default")
            self.font = self.dpy.open_font("fixed")
            if self.font == None:
                print("Failed to set font")
                exit(2)
        self.draw_context.font = self.font
        q = self.font.query()
        self.draw_context.font_properties['font_ascent'] = q.font_ascent
        self.draw_context.font_properties['font_descent'] = q.font_descent
        self.draw_context.font_properties['font_height'] = \
            q.font_ascent + q.font_descent
        self.draw_context.font_properties['char_width'] = \
            q.char_infos[0]['character_width']
        self.bar_height = self.draw_context.font_properties['font_height'] + \
            self.config.bar_padding
        self.draw_context.h = self.bar_height

        self.update_bars()
        self.scan()

    def apply_size_hints(self, client, x, y, width, height, interact):
        # TODO: Figure out what is going on with interact.
        if width < 1:
            width = 1
        if height < 1:
            height = 1

        #if interact:
        if x > self.screen_width:
            x = self.screen_width - client.width
        if y > self.screen_height:
            y = self.screen_height - client.height
        if x + client.width + (2 * self.config.border_width) < 0:
            x = 0
        if  y + client.width + (2 * self.config.border_width) < 0:
            y = 0
        # else:
        if height < self.bar_height:
            height = self.bar_height

        if width < self.bar_height:
            width = self.bar_height

        # XXX: This section in dwm.c also accounts for floating
        # XXX: and "!c->mon->lt[c->mon->sellt]->arrange"
        if self.config.respect_resize_hints:
            client_hints = client.get_wm_normal_hints()
            base_is_min = client_hints.base_width == client_hints.min_width \
                and client_hints.base_height == client_hints.min_height
            if not base_is_min:
                width = width - client_hints.base_width
                height = height - client_hints.base_height

            if client_hints.min_aspect > 0 and client_hints.max_aspect > 0:
                if client_hints.max_aspect < (width / height):
                    width = height * client_hints.max_aspect + 0.5
                elif client_hints.min_aspect < (width / height):
                    height = width * client_hints.min_aspect + 0.5

            if base_is_min:
                width = width - client_hints.base_width
                height = height - client_hints.base_height

            if client_hints.height_inc:
                height = height % client_hints.height_inc
            if client_hints.width_inc:
                width -= width % client_hints.width_inc

            if width + client_hints.base_width > client_hints.min_width:
                width -= width % client_hints.base_width
            else:
                width = client_hints.min_width

            if height + client_hints.base_height > client_hints.min_height:
                height = height + client_hints.base_height
            else:
                height = client_hints.min_height

        return x != client.x or y != client.y or \
             width != client.width or width != client.height

    def banish_pointer(self):
         self.root.warp_pointer(self.screen_width, self.screen_height)
         self.dpy.sync()
    
    def cycle_focus(self, direction):
        cur_client_index = self.current_client
        clients = self.clients[self.config.tags[self.current_tag]] 

        #get the next or previous window in the stack
        if direction.lower() == 'next':
            adjusted_client_index = cur_client_index + 1
        elif direction.lower() in ('prev', 'previous', 'last'):
            adjusted_client_index = cur_client_index - 1


        if adjusted_client_index == -1:
            adjusted_client_index = len(clients) - 1
        elif adjusted_client_index == len(clients):
            adjusted_client_index = 0

        adjusted_client = clients[adjusted_client_index]
        cur_client = clients[cur_client_index]


        self.set_inactive_border(cur_client.window)
        adjusted_client.is_focused = True
        cur_client.is_focused = False

        self.focus(adjusted_client.window)

        ##raise window (really only needed for float/monocle, but still...)

    def destroy(self, client):
        client.window.destroy()
        self.scan()

    def destroy_current(self):
        cur_client = self.current_client
        if cur_client is not None:
            self.destroy(self.clients[self.config.tags[self.current_tag]][cur_client])

    @property
    def current_client(self):
        cur_client = None
        for client in self.clients[self.config.tags[self.current_tag]]:
            if client.is_focused:
                cur_client = self.clients[self.config.tags[self.current_tag]].index(client)
                break
      
        #Don't break if a client isn't marked as focused, just use master
        if cur_client is None:
            print "No focus found; Using master..."
            for client in self.clients[self.config.tags[self.current_tag]]:
                if client.is_master:
                    cur_client = self.clients[self.config.tags[self.current_tag]].index(client)
                    break

        return cur_client 

    def draw_bar(self):
        # TODO: add support for status bar
        y_offset = self.bar_height - \
            self.draw_context.font_properties['font_height'] / 2
        x_offset = 3

        act_fg = self.colormap.alloc_named_color(self.config.colors[1][1]).pixel
        act_bg = self.colormap.alloc_named_color(self.config.colors[1][2]).pixel

        norm_fg = self.colormap.alloc_named_color(self.config.colors[0][1]).pixel
        norm_bg = self.colormap.alloc_named_color(self.config.colors[0][2]).pixel
        # draw tag names
        for tag in self.config.tags:
            if tag is self.config.tags[self.current_tag]:
                self.gc.change(foreground = act_fg,
                                background = act_bg)
                self.bar_window.draw_text(self.gc, x_offset, y_offset, tag)
                x_offset += self.draw_context.font_properties['char_width'] * \
                    len(tag) + 3

                #make sure you change the gc back
                self.gc.change(foreground = norm_fg,
                                background = norm_bg)
            else:
                self.bar_window.draw_text(self.gc, x_offset, y_offset, tag)
                x_offset += self.draw_context.font_properties['char_width'] * \
                    len(tag) + 3

        # TODO: change this to support multiple layouts on different tags
        self.bar_window.draw_text(self.gc, x_offset, y_offset, \
            self.config.layouts[int(self.current_layout)][0])
        x_offset += self.draw_context.font_properties['char_width'] * \
            len(self.config.layouts[self.current_layout][0])

    def focus(self, window):
        window.set_input_focus(X.RevertToParent, X.CurrentTime)
        window.configure(stack_mode = X.Above)
        self.set_active_border(window)

    def manage_clients(self):
        for tag in self.clients:
            if len(self.clients[tag]) > 1:
                #we have clients other than the bar_window
                for client in self.clients[tag]:
                    geometry = client.window.get_geometry()
                    attributes = client.window.get_attributes()
                    client.x = client.old_x = geometry.x
                    client.y = client.old_y = geometry.y
                    client.w = client.old_w = geometry.width
                    client.h = client.old_h = geometry.height
                    client.border_width = self.config.border_width

            #trickery! (Call the layout function dynamically)
            layout = getattr(Window_Manager, self.config.layouts[self.current_layout][2])
            layout(self)

    def monocle(self):
        """Monocle layout - resize all windows to (screen_height - bar_height - gap?)"""
        if len(self.clients[self.config.tags[self.current_tag]]) > 0:
            for client in self.clients[self.config.tags[0]]:
                self.resize(    client, 
                                self.config.gap_width, 
                                self.bar_height + self.config.gap_width,
                                self.screen_width - \
                                    (2 * self.config.border_width) - \
                                    (2 * self.config.gap_width),
                                self.screen_height - \
                                    (2 * self.config.border_width) - \
                                    (2 * self.config.gap_width) - \
                                    self.bar_height, 
                                False)
                if client is self.clients[self.config.tags[0]][-1]:
                    client.is_master = True
                    client.is_visible = True
                    self.focus(client.window)
                else:
                    client.is_master = False
                    client.is_visible = False

    def parse_keypress(self,ev):
        """Handles keypress events after we've received an event for them"""
        for key in self.config.keys:
            try:
                modmask = reduce(operator.ior, 
                                [getattr(X, "{0}Mask".format(x)) 
                                    for x in key['mods'].split()])
            except:
                modmask = 0
            if modmask & ev.state == modmask:
                keycode = self.dpy.keysym_to_keycode(key['key'])
                print keycode
                print ev.detail
                print keycode == ev.detail
                if keycode == ev.detail:
                    if key['type'] == 'spawn':
                        self.spawn(key['action']);
                    elif key['type'] == 'focus':
                        self.cycle_focus(key['action'])
                    else:
                        #this handles anything that doesn't require an argument
                        tmp = getattr(self, key['type'])
                        tmp()

    def resize(self, client, x, y, width, height, interact):
        if (self.apply_size_hints(client, x, y, width, height, interact)):
            self.resize_client(client, x, y, width, height)

    def resize_client(self, client, x, y, width, height):
        client.old_x = client.x
        client.old_y = client.y
        client.old_width = client.width
        client.old_height = client.height
        client.x = x + self.config.gap_width
        client.y = y + self.config.gap_width
        client.width = width - (x + width + 2)
        client.height = height - (y + height + 2)
        
        client.window.configure(x=x, y=y, width=width, height=height)

    def restack(self):
        self.update_bar()
        sibling = self.bar_window
        for client in self.clients[self.config.tags[self.current_tag]]:
            if not client.is_floating:
                client.window.configure(stack_mode = X.Below,
                    sibling = sibling)
                sibling = client.window
        self.dpy.sync()
        #self.dpy.check_mask_event(X.Enter_Window_Mask)

    def scan(self):
        self.clients[self.config.tags[self.current_tag]] = []
        tree = self.root.query_tree()
        if tree:
            for window in tree.children:
                try:
                    wa = window.get_attributes()
                except:
                    continue

                if not wa.override_redirect:
                    border_color = self.colormap.alloc_named_color(\
                        self.config.colors[0][0]).pixel
                    window.configure(border_width=self.config.border_width)
                    window.change_attributes(None,border_pixel=border_color)

                if wa.override_redirect or window.get_wm_transient_for():
                    continue
                if wa.map_state == X.IsViewable or window.get_wm_state() == Xutil.IconicState:
                    self.clients[self.config.tags[self.current_tag]].append(Client(window))
                    #self.manage(window,wa)

            #now the transients
            for window in tree.children:
                try:
                    wa = window.get_attributes()
                except:
                    continue
                transient_for = window.get_wm_transient_for()
                if transient_for and (wa.map_state == X.IsViewable or \
                    window.get_wm_state() == X.IconicState):
                    self.clients[self.config.tags[0]].append(Client(window))
        self.dpy.sync()
        self.manage_clients()

    def set_inactive_border(self, window):
        border_color = self.colormap.alloc_named_color(\
            self.config.colors[0][0]).pixel
        window.change_attributes(None,border_pixel=border_color )
        self.dpy.sync()
    
    def set_active_border(self, window):
        border_color = self.colormap.alloc_named_color(\
            self.config.colors[1][0]).pixel
        window.change_attributes(None,border_pixel=border_color )
        self.dpy.sync()

    def setup_keybinds(self):
        """parses user's keybinds and sets the global keybind up
        including all ignored masks"""
        def super_set(i):
            return chain.from_iterable(combinations(i, r) for r in range(len(i) + 1))

        for key in self.config.keys:
            try:
                modmask = reduce(operator.ior, [getattr(X, "{0}Mask".format(x)) 
                                                        for x in key['mods'].split()])
            except:
                modmask = 0

            keycode = self.dpy.keysym_to_keycode(key['key'])
            print keycode
            for ignored in super_set([X.Mod2Mask, X.LockMask, X.Mod5Mask]):
                ignored = reduce(lambda x, y: x | y, ignored, 0)
                self.root.grab_key(keycode, modmask | ignored, 1, X.GrabModeAsync, X.GrabModeAsync)
            self.dpy.sync()

    def spawn(self, to_run):
        """Launch program associated with keybind, but don't wait for it to
        complete before moving on"""
        if len(to_run) > 1:
            spawnvp(P_NOWAIT,to_run[0],to_run)
        else:
            spawnvp(P_NOWAIT,to_run,to_run)

    def tile(self):
        # TODO: figure out why master needs 3*gap_width to get proper size


        if len(self.clients[self.config.tags[self.current_tag]]) > 0:
            #if there's only one client, call monocle
            if len(self.clients[self.config.tags[0]]) <= 1:
                self.monocle()
                return
            
            master_width = (self.config.mfact * self.screen_width) + \
                                self.config.gap_width + \
                                (2*self.config.border_width)
            slave_width = self.screen_width - master_width

           #search for master window - if none, make one
            master_found = False
            for client in self.clients[self.config.tags[self.current_tag]]:
                if client.is_master:
                    master_found = True
                    break
            if not master_found:
                self.clients[self.config.tags[self.current_tag]][0].is_master = True
                self.set_active_border(self.clients[self.config.tags[self.current_tag]][0].window)

            num_slaves = len(self.clients[self.config.tags[self.current_tag]])-1
            slave_height = self.screen_height*(1/num_slaves)
            counter = 0

            for client in self.clients[self.config.tags[self.current_tag]]:
                client.is_visible = True
                if client.is_master:
                    self.resize(
                        client,
                        self.config.gap_width + self.config.border_width,
                        self.bar_height + self.config.border_width + 
                            self.config.gap_width,
                        master_width - (2*self.config.border_width) - \
                            self.config.gap_width,
                        (self.screen_height - self.bar_height) - \
                            (2 * self.config.border_width) - \
                            (3 * self.config.gap_width),
                        False)
                    self.focus(client.window)
                    #client.window.set_input_focus(X.RevertToParent, X.CurrentTime)
                else:
                    if not counter:
                        #first / only slave
                        self.resize(
                            client,
                            master_width + self.config.border_width + 
                                self.config.gap_width,
                            self.bar_height + self.config.border_width + 
                                self.config.gap_width,
                            slave_width - (3 * self.config.gap_width) - \
                                          (3 * self.config.border_width),
                            slave_height - (5 * self.config.border_width) - \
                                           (5 * self.config.gap_width),
                            False)
                    else:
                        #print "multiple slaves"
                        self.resize(client,
                            master_width + self.config.border_width + self.config.gap_width,
                            (counter*slave_height) + self.config.border_width + self.bar_height,
                            slave_width,
                            slave_height - self.config.border_width,
                            False)
                    counter += 1

    def update_bars(self):
        """draws bar window. bar text drawn in self.draw_bar"""
        if self.config.show_bar and not self.bar_window:
            bar_height = self.draw_context.font_properties['font_height'] + \
                self.config.bar_padding
            if self.config.top_bar:
                bar_x = 0
            else:
                bar_x = self.screen_height - bar_height

            self.colormap = self.screen.default_colormap
            norm_fg = self.colormap.alloc_named_color(\
                self.config.colors[0][1]).pixel
            norm_bg = self.colormap.alloc_named_color(\
                self.config.colors[0][2]).pixel

            self.bar_window = self.root.create_window(
                bar_x, 0, self.screen_width, bar_height, 0,
                self.screen.root_depth,
                background_pixel=norm_bg,
                event_mask=X.ExposureMask | X.KeyPressMask | X.CWOverrideRedirect)
            self.bar_window.change_attributes(override_redirect=True)
            self.bar_window.map()

            self.gc = self.bar_window.create_gc(
                foreground=norm_fg,
                background=norm_bg,
                font=self.font)

        self.draw_bar()

    def update_title(self, win):
        #TODO: Make this <strike>more efficient</strike> work
        for client in self.clients:
            if client.window == win:
                client.title = win.get_wm_name()

    def run(self):
        self.setup_keybinds()
        current_client = self.clients[self.config.tags[self.current_tag]][0]
        self.focus(current_client.window)
        while True:
            ev = self.dpy.next_event()
            if ev.type == X.Expose:
                pass
            elif ev.type == X.MapNotify or ev.type == X.UnmapNotify:
                self.scan()
                self.draw_bar()
            elif ev.type == X.KeyPress:
                self.parse_keypress(ev)
            elif ev.type == X.EnterNotify:
                self.set_active_border(ev.window)
            elif ev.type ==  X.FocusIn or ev.type == X.FocusOut:
                self.set_active_border(ev.window)
            else:
                #print "Uncaught event: ",ev.type
                pass


if __name__ == "__main__":
    win = Window_Manager()
    win.run()
