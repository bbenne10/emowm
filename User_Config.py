from Xlib import X
from keysymdef import keysyms

class User_Config():
    def __init__(self):

        self.font = "-xos4-terminus-medium-r-normal--12-120-72-72-c-60-iso8859-1"
        self.colors = [
            # BORDER     FG        BG
            ['#1b1d1e', '#a0a0a0', '#1b1d1e'],  # Normal
            ['#b6e354', '#b6e354', '#1b1d1d'],  # Selected
            ['#fd971f', '#fd971f', '#1b1d1e']   # Urgent / Warning
            ]
        self.border_width = 2  # px
        self.gap_width = 3     # px
        self.snap = 32         # px
        self.show_bar = True
        self.top_bar = True

        self.tags = ['WEB', 'CODE', 'MUSIC', 'TERM', 'CHAT']
        self.rules = []
        self.mfact = 0.5
        self.nmaster = 1
        self.respect_resize_hints = False
        self.layouts = [
            # Symbol     Gaps        Layout type
            ['[T]',     True,       'tile'],
            #['[F]',     False,      'float'],
            ['[M]',     True,       'monocle']
            ]

        self.keys = [
            # INTERNAL FUNCTIONS
            {   'mods': "Mod4",
                'key' : keysyms['t'],
                'type': 'tile'
            },
            {   'mods': "Mod4",
                'key' : keysyms['m'],
                'type': 'monocle'
            },
            {   'mods': "Mod4",
                'key' : keysyms['j'],
                'type': 'focus',
                'action' : 'prev'
            },
            {   'mods': "Mod4",
                'key' : keysyms['k'],
                'type' : 'focus',
                'action' : 'next'
            },
            {   'mods': "Mod4 Shift",
                'key' : keysyms['c'],
                'type': 'destroy_current'
            },
            {   'mods': 'Control Shift',
                'key' : keysyms['b'],
                'type': 'banish_pointer'
            },

            # EXTERNAL LAUNCHING
            {   'mods': "Mod4 Shift",
                'key':  keysyms['Return'],
                'type': 'spawn',
                'action': ['urxvt', '-fn','fixed']
            },
            {   'mods': "Mod4",
                'key':  keysyms['p'],
                'type': 'spawn',
                'action': ['dmenu_run','-fn',self.font,'-nb',self.colors[0][2],
                           '-nf',self.colors[0][1],'-sb',self.colors[1][2],
                           '-sb',self.colors[1][1]]
            },
            {   'mods': '0',
                'key' :  keysyms['XF86AudioMute'],
                'type' : 'spawn',
                'action' : ['amixer','set','Master','toggle']
            },
            {   'mods': '0',
                'key' :  keysyms['XF86AudioLowerVolume'],
                'type' : 'spawn',
                'action' : ['amixer','set','Master','5%-']
            },
            {   'mods': '0',
                'key' :  keysyms['XF86AudioRaiseVolume'],
                'type' : 'spawn',
                'action' : ['amixer','set','Master','5%+']
            },
            {   'mods': '0',
                'key' :  keysyms['XF86AudioPlay'],
                'type' : 'spawn',
                'action' : ['ncmpcpp','toggle']
            },
            {   'mods': '0',
                'key' :  keysyms['XF86AudioStop'],
                'type' : 'spawn',
                'action' : ['ncmpcpp','stop']
            },
            {   'mods': '0',
                'key' :  keysyms['XF86AudioPrev'],
                'type' : 'spawn',
                'action' : ['ncmpcpp','prev']
            },
            {   'mods': '0',
                'key' :  keysyms['XF86AudioNext'],
                'type' : 'spawn',
                'action' : ['ncmpcpp','next']
            },
            {   'mods': '0',
                'key' :  keysyms['XF86Sleep'],
                'type' : 'spawn',
                'action' : ['sudo','pm-suspend']
            }
        ]

        self.buttons = []
        self.xinerama = False
        self.bar_padding = 8
